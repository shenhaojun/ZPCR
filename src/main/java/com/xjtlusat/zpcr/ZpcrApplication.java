package com.xjtlusat.zpcr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZpcrApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZpcrApplication.class, args);
    }

}
