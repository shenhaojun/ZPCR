package com.xjtlusat.zpcr.service.implement;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xjtlusat.zpcr.dao.CarMapper;
import com.xjtlusat.zpcr.dao.ImageMapper;
import com.xjtlusat.zpcr.entity.Car;
import com.xjtlusat.zpcr.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    CarMapper carMapper;

//    @Autowired
//    ImageMapper imageMapper;

    @Override
    public String insert(Car car) {
        carMapper.insert(car);
        return "redirect:/cars_admin/selectCar";
    }

    @Override
    public String updateById(Car car) {
        carMapper.updateById(car);
        return "redirect:/cars_admin/selectCar";
    }

    @Override
    public String deleteById(Long id) {
        carMapper.deleteById(id);
        return "redirect:/cars_admin/selectCar";
    }

    @Override
    public String selectPage(Integer pageNumber, Integer pageSize, String search, Model model) {
        LambdaQueryWrapper<Car> wrapper = Wrappers.<Car>lambdaQuery();
        if(StrUtil.isNotBlank(search)) { // 防止该字段在数据库中未设置
            wrapper.like(Car::getType, search);
        }
        Page<Car> carPage = carMapper.selectPage(new Page<>(pageNumber, pageSize), wrapper);
        model.addAttribute("data", carPage);
        return "cars_admin";
    }
}
