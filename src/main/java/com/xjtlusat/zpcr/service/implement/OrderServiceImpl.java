package com.xjtlusat.zpcr.service.implement;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xjtlusat.zpcr.dao.OrderMapper;
import com.xjtlusat.zpcr.entity.Order;
import com.xjtlusat.zpcr.service.OrderService;
import com.xjtlusat.zpcr.util.OrderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Override
    public String generate(Order order) {
        order.setId(OrderUtil.get16UUID());
        order.setStartTime(OrderUtil.getCurrentTimestamp());
        order.setPrice(0.0);
        orderMapper.insert(order);
        return "redirect:/car_details";
    }

    @Override
    public String insert(Order order) {
        order.setId(OrderUtil.get16UUID());
        orderMapper.insert(order);
        return "redirect:/orders_admin/selectOrder";
    }

    @Override
    public String updateById(Order order) {
        orderMapper.updateById(order);
        return "redirect:/orders_admin/selectOrder";
    }

    @Override
    public String deleteById(Long id) {
        orderMapper.deleteById(id);
        return "redirect:/orders_admin/selectOrder";
    }

    @Override
    public String selectPage(Integer pageNumber, Integer pageSize, String search, Model model) {
        LambdaQueryWrapper<Order> wrapper = Wrappers.<Order>lambdaQuery();
        if(StrUtil.isNotBlank(search)) { // 防止该字段在数据库中未设置
            wrapper.like(Order::getId, search);
        }
        Page<Order> orderPage = orderMapper.selectPage(new Page<>(pageNumber, pageSize), wrapper);
        model.addAttribute("data", orderPage);
        return "orders_admin";
    }
}
