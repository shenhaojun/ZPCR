package com.xjtlusat.zpcr.service;

import com.xjtlusat.zpcr.entity.Order;
import org.springframework.ui.Model;

public interface OrderService {
    String generate(Order order);
    String insert(Order order);
    String updateById(Order order);
    String deleteById(Long id);
    String selectPage(Integer pageNumber, Integer pageSize, String search, Model model);
}
