package com.xjtlusat.zpcr.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xjtlusat.zpcr.entity.User;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface UserInfoService {
    User selectByNameAndPass(String username, String password);
    User selectById(int id);
    String insert(User user);
    String updateById(User user);
    String deleteById(Long id);
    String selectPage(Integer pageNumber, Integer pageSize, String search, Model model);
    Page<User> getPage(Integer pageNumber, Integer pageSize, String search);

}
