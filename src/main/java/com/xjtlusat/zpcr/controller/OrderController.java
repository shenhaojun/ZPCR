package com.xjtlusat.zpcr.controller;

import com.xjtlusat.zpcr.entity.Order;
import com.xjtlusat.zpcr.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("orders")
public class OrderController {

    @Autowired
    OrderService orderService;
    @RequestMapping()
    public String ordersPage(Model model) {
        model.addAttribute("t", "123");
        return "orders";
    }

    @RequestMapping("generate")
    public String generate(Order order) {
        return orderService.generate(order);
    }
}
