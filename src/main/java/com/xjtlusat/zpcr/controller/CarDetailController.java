package com.xjtlusat.zpcr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("car_details")
public class CarDetailController {

    @RequestMapping
    public ModelAndView carDetailPage() {
        return new ModelAndView("car_details");
    }

}
