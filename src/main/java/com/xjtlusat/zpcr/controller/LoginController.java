package com.xjtlusat.zpcr.controller;

import com.xjtlusat.zpcr.entity.User;
import com.xjtlusat.zpcr.service.UserInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("login")
public class LoginController {

    @Resource
    private UserInfoService userInfoService;

    @RequestMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User user = userInfoService.selectByNameAndPass(username, password);
        if(user == null) {
            return "login";
        }
        return "redirect:/users";
    }

    @RequestMapping
    public String loginPage() {
        return "login";
    }
}
