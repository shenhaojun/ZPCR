package com.xjtlusat.zpcr.controller;

import com.xjtlusat.zpcr.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("cars")
public class CarController {

    @Autowired
    CarService carService;

    @RequestMapping
    public ModelAndView carsPage() {
        return new ModelAndView("cars");
    }


}
