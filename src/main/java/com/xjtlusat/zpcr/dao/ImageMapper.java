package com.xjtlusat.zpcr.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xjtlusat.zpcr.entity.Image;
import org.apache.ibatis.annotations.Mapper;

//@Mapper
public interface ImageMapper extends BaseMapper<Image> {

}
